var gulp = require('gulp'),
    less = require('gulp-less'), //less to css
    minify = require('gulp-minify-css'), //minify css
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload');

var paths = {
    'dev': {
      'less': './assets/less/*.less',
      'js': './assets/js/',
      'vendor': './libs/'
    },
    'assets': {
      'css': './assets/css/',
      'js': './assets/js/',
      'vendor': './libs/'
    }
};

//tasks
//css
gulp.task('styles', function() {

  return gulp.src(paths.dev.less)
        .pipe(less())
        .pipe(gulp.dest(paths.assets.css))
        .pipe(minify({keepSpecialComments:0}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.assets.css))
        .pipe(livereload());
});

//watch
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(paths.dev.less, ['styles']);
});

//default
gulp.task('default', ['styles', 'watch']);
