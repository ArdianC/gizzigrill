

$(window).load(function() {
	$(".loader").delay(500).fadeOut();
	$("#mask").delay(1000).fadeOut("slow");
});

(function($){
  $(window).load(function() {
    $('html').niceScroll({
      cursorcolor: "#ffcb08",
      cursorborderradius: "2px"
    });
  });

	$('.trigger-zoom').on('click', function(e) {
			e.preventDefault();
			$('.page').toggleClass('zoom-active');
            $(this).toggleClass('active');
            $('.trigger-zoom i').toggleClass('fa-search-minus fa-search-plus');


			if(!$('.page').hasClass('zoom-active')) {
				$('.page img').removeClass('zoomImg').removeAttr('style');
			}

			$.each($('.zoom-active'), function(index, value) {
				var page_url = 'http://localhost/gizzigrill/';
				$(value).zoom({
					on: 'mouseover',
					url: page_url + $(this).data('imgsrc'),
					callback: function() {
					}
				})
			});
	});
}(jQuery));

$(document).ready(function() {
	$('.navbar-right ul li a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});

	 $("#mobile-menu-slider").owlCarousel({
			 loop: true,
       autoplay: true,
		   autoplayTimeout: 6000,
       items: 1,
       itemsDesktop: [1199, 1],
       itemsDesktopSmall: [979, 1]
   });

  //slider
  $('.home-slider').pogoSlider({
    autoplay: true,
		autoplayTimeout: 5000,
		displayProgess: false,
		generateNav: false,
		preserveTargetSize: true,
		targetWidth: 1000,
		targetHeight: 300,
    pauseOnHover: true,
		responsive: true
  });

	$('.swipebox' ).swipebox();
});

//gallery
/**** GALLERY ****/

$(window).load(function() {
  $(document).ready(function(){
      jQuery('a[data-gal]').each(function() {
      jQuery(this).attr('rel', jQuery(this).data('gal'));
  });
  $("a[data-rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',theme:'light_square',slideshow:false,overlay_gallery: false,social_tools:false,deeplinking:false});
  });

  var $container = $('.portfolio');
  $container.isotope({
      filter: '*',
      animationOptions: {
          duration: 750,
          easing: 'linear',
          queue: false,
      }
  });

  $('nav.primary ul a').click(function(){
      var selector = $(this).attr('data-filter');
      $container.isotope({
          filter: selector,
          animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false,
          }
      });
    return false;
  });

  var $optionSets = $('nav.primary ul'),
     $optionLinks = $optionSets.find('a');

     $optionLinks.click(function(){
        var $this = $(this);
    // don't proceed if already selected
    if ( $this.hasClass('selected') ) {
        return false;
    }
  var $optionSet = $this.parents('nav.primary ul');
  $optionSet.find('.selected').removeClass('selected');
  $this.addClass('selected');
  });
    if (!$('#ip-container').hasClass('single')) {
        $('#nav li').removeClass('current');
        $('#nav li:first-child').addClass('current');
    }
}); // End Window Load

//Set home slider height on resize
$(window).resize(function () {
    $('#home-slider').height($(window).height());
    $('.slider-parallax').css('padding-top', $(window).height() + 'px');
});


var pages = $('.pages').children();
var grabs = false; // Gonna work on this, one day

pages.each(function(i) {
  var page = $(this);
  if (i % 2 === 0) {
    page.css('z-index', (pages.length - i));
  }
});

$(window).load(function() {

  $('.page').click(function() {
    var page = $(this);
    var page_num = pages.index(page) + 1;
    if (page_num % 2 === 0) {
      page.removeClass('flipped');
      page.prev().removeClass('flipped');
    } else {
      page.addClass('flipped');
      page.next().addClass('flipped');
    }
  });

  if (grabs) {
    $('.page').on('mousedown', function(e) {
      var page = $(this);
      var page_num = pages.index(page) + 1;
      var page_w = page.outerWidth();
      var page_l = page.offset().left;
      var grabbed = '';
      var mouseX = e.pageX;
      if (page_num % 2 === 0) {
        grabbed = 'verso';
        var other_page = page.prev();
        var centerX = (page_l + page_w);
      } else {
        grabbed = 'recto';
        var other_page = page.next();
        var centerX = page_l;
      }

      var leaf = page.add(other_page);

      var from_spine = mouseX - centerX;

      if (grabbed === 'recto') {
        var deg = (90 * -(1 - (from_spine/page_w)));
        page.css('transform', 'rotateY(' + deg + 'deg)');

      } else {
        var deg = (90 * (1 + (from_spine/page_w)));
        page.css('transform', 'rotateY(' + deg + 'deg)');
      }

      leaf.addClass('grabbing');

      $(window).on('mousemove', function(e) {
        mouseX = e.pageX;
        if (grabbed === 'recto') {
          centerX = page_l;
          from_spine = mouseX - centerX;
          var deg = (90 * -(1 - (from_spine/page_w)));
          page.css('transform', 'rotateY(' + deg + 'deg)');
          other_page.css('transform', 'rotateY(' + (180 + deg) + 'deg)');
        } else {
          centerX = (page_l + page_w);
          from_spine = mouseX - centerX;
          var deg = (90 * (1 + (from_spine/page_w)));
          page.css('transform', 'rotateY(' + deg + 'deg)');
          other_page.css('transform', 'rotateY(' + (deg - 180) + 'deg)');
        }

        console.log(deg, (180 + deg) );
      });


      $(window).on('mouseup', function(e) {
        leaf
          .removeClass('grabbing')
          .css('transform', '');

        $(window).off('mousemove');
      });
    });
  }

  $('.book').addClass('bound');
});
