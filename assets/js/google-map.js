/* googleMaps Footer Map */
  var gold = "#BDA86B";
  var currentColor = 'gold';
  var color = gold;
  var saturation = 100;
  var drag;
	if($(window).width()<796){drag=false;}else{drag=true;}

  $('.picker-gold').click(function(){
		$('body').removeClass(currentColor);
		$('body').addClass('gold');
		currentColor='gold';
		wpgmappity_maps_loaded();
	});


  function wpgmappity_maps_loaded() {
    var pointerUrl = 'assets/images/pin.png';
    switch(currentColor) {
        case ('bluegray'):
        var color = bluegray;
      var saturation = 100;
              break;
          case ('black'):
              var color = black;
              var saturation = -100;
              break;
          case ('green'):
              var color = green;
              var saturation = 100;
              break;
          case ('yellow'):
              var color = yellow;
      var saturation = 100;
              break;
          case ('gold'):
              var color = gold;
      var saturation = 100;
              break;
          case ('red'):
              var color = red;
      var saturation = 100;
              break;
          case ('orange'):
              var color = orange;
      var saturation = 100;
              break;
          case ('wine'):
              var color = wine;
      var saturation = 100;
              break;
      }
  var latlng = new google.maps.LatLng(42.626705,21.1475552);
  var styles = [
    {
      "featureType": "landscape",
      "stylers": [
        {"hue": "#000"},
        {"saturation": -100},
        {"lightness": 40},
        {"gamma": 1}
      ]
    },
    {
      "featureType": "road.highway",
      "stylers": [
        {"hue": color},
        {"saturation": saturation},
        {"lightness": 20},
        {"gamma": 1}
      ]
    },
    {
      "featureType": "road.arterial",
      "stylers": [
        {"hue": color},
        {"saturation": saturation},
        {"lightness": 20},
        {"gamma": 1}
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {"hue": color},
        {"saturation": saturation},
        {"lightness": 50},
        {"gamma": 1}
      ]
    },
    {
      "featureType": "water",
      "stylers": [
        {"hue": "#000"},
        {"saturation": -100},
        {"lightness": 15},
        {"gamma": 1}
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {"hue": "#000"},
        {"saturation": -100},
        {"lightness": 25},
        {"gamma": 1}
      ]
    }
  ];
  var options = {
   center : latlng,
   mapTypeId: google.maps.MapTypeId.ROADMAP,
   zoomControl : false,
   mapTypeControl : false,
   scaleControl : false,
   streetViewControl : false,
   draggable:drag,
   scrollwheel:false,
   panControl : false, zoom : 14,
   styles: styles
  };
  var wpgmappitymap = new google.maps.Map(document.getElementById('wpgmappitymap'), options);
  var point0 = new google.maps.LatLng(42.626705,21.1475552);
  var marker0= new google.maps.Marker({
   position : point0,
   map : wpgmappitymap,
   icon: pointerUrl //Custom Pointer URL
   });
  google.maps.event.addListener(marker0,'click',
   function() {
   var infowindow = new google.maps.InfoWindow(
   {content: '<div class="infowindow"><figure class="pull-left"><img src="./assets/images/gizzi-rest.png" /></figure><div class="info-content pull-right">Gizzi Grill</div></div>'});
   infowindow.open(wpgmappitymap,marker0);
   });
  }
  window.onload = function() {
   wpgmappity_maps_loaded();
  };
